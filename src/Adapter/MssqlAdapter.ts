import { Knex, knex }                                        from 'knex';
import { Adapter, LogLevel, OmniORM, Repository, ClassType } from 'omni-orm';

import { KnexMssqlConfig } from '../Interface/KnexMssqlConfig';
import { KnexQuery }       from '../Interface/KnexQuery';
import { MssqlRepository } from '../Repository/MssqlRepository';

export class MssqlAdapter implements Adapter {
	dialect = 'mssql';
	orm!: OmniORM<MssqlAdapter>;
	knex!: Knex;

	config: KnexMssqlConfig;

	constructor(config: KnexMssqlConfig) {
		this.config = config;
	}

	setORM(orm: OmniORM<MssqlAdapter>) {
		this.orm = orm;
		return this;
	}

	async connect(): Promise<boolean> {
		try {
			this.knex = knex({
				client: 'mssql',
				log: {
					error: (message: string) => {
						this.orm.log(LogLevel.Error, 'knex-error', message);
					},
					debug: (message: string) => {
						this.orm.log(LogLevel.Debug, 'knex-debug', message);
					},
					deprecate: (method: string, alternative: string) => {
						this.orm.log(LogLevel.Warning, 'knex-debug', method, alternative);
					},
					warn: (message: string) => {
						this.orm.log(LogLevel.Warning, 'knex-warning', message);
					}
				},
				connection: {
					host: this.config.host,
					user: this.config.user,
					database: this.config.database,
					port: this.config.port,
					password: this.config.pass,
					timezone: 'UTC',
					dateStrings: true,
					ssl: this.config.ssl,
					options: {
						useUTC: true,
						enableArithAbort: true
					},
					connectionTimeout: this.config.connectionTimeout ?? 5000
				}
			});

			this.knex.on('query', (query: KnexQuery) => {
				this.orm.log(LogLevel.Verbose, 'query', query.sql, query.bindings, query);
			});

			this.knex.on('query-response', (response: any, query: KnexQuery) => {
				this.orm.log(LogLevel.Debug, 'query-response', query.sql, query.bindings, query);
			});

			this.knex.on('query-error', (error: Error, query: KnexQuery) => {
				this.orm.log(LogLevel.Error, 'query-response', query.sql, query.bindings);
			});

			this.knex.on('disconnect', (error: Error) => {
				this.orm.log(LogLevel.Error, 'query-response', error.message, error);
			});

			this.orm.emit('connected');
			return true;
		}
		catch (e) {
			console.log(e);
			return false;
		}
	}

	async disconnect(): Promise<boolean> {
		try {
			await this.knex.destroy();
			this.orm.emit('disconnected');
			return true;
		}
		catch (e) {
			console.log(e);
			return false;
		}
	}

	createRepository<T extends Record<string, any>>(classType: ClassType<T>): Repository<T> {
		return new MssqlRepository<T>(classType, this.orm);
	}
}
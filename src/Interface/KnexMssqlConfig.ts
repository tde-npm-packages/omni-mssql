export interface KnexMssqlConfig {
	user?: string;
	pass?: string;
	port: number;
	database: string;
	schema?: string;
	host: string;
	ssl?: boolean;
	connectionTimeout?: number;
}